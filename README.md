RequestCEP
==========

É um simples módulo que foi desenvolvido com o objetivo de facilitar a obtenção de dados do CEP através do website da Correios.

### Instalação

Via PIP:

```sh
pip install requestcep
```

### Como utilizar

Importe o módulo da seguinte forma:

```python
from requestcep import RequestCep
```

Para obter os dados de um específico CEP:

```python
rc = RequestCep()
data = rc.search(cep="99999999")
print(data)
```

Para pesquisar e baixar todos os CEPs disponíveis:

```python
rc = RequestCep()
rc.download_all()
```

Executando o código acima, será um criado um banco chamado `ceps.shelf` onde o script foi executado. Caso queira mudar o diretório, use o parâmetro `dbfilepath`:

```python
rc = RequestCep()
rc.download_all(dbfilepath="./foo/bar.shelf")
```

Também é possível exportar todos os CEPs dentro do banco em um arquivo no formato JSON.

```python
from requestcep import generate_json

generate_json()
```

Se você alterou a localização do banco de dados, use o parâmetro `dbfilepath`:

```python
generate_json(dbfilepath="./foo/bar.shelf")
```

Também é possível alterar a localização de onde o arquivo JSON será gerado com o parâmetro `filepath`:

```python
generate_json(filepath="./foo/bar.json")
```
